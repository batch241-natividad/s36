//Contains all the endpoints for our application
//We seperate the routes such that "index.js" only contains information on the server

const express = require("express");

//Creates a router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../Controllers/taskController")


//Routes
//The routes are responsible for defining the URIs that our client accesses and the corresponding  controller function that will be used when a route is accessed

//They invoke the controller functions from the controller files
//All the business logic is done in the controller.

//Route to GET ALL THE TASK

router.get("/:id", (req, res) => {

	const taskId = req.params.id;
	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Route for creating a NEW TASK 

router.post("/", (req, res) => {


	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for DELETING A TASK

router.delete("/:id",  (req, res) => {

	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


//Route for Updating a task 
router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



//Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;
