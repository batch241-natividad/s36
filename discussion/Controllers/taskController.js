//Controllers contains the functions and business logic of our Express JS application

//Meaning all the operations it can do will be placed in this file


//Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations 
//Allows us to use the contents of the "task.js" file in the "models" folder 
const Task = require("../models/task");

//Controller function for GETTING ALL THE TASKS
//Defines the functions to be used in the "taskRoutes.js" file and export these functions

module.exports.getOneTask = (taskId) => {

	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.findById(taskId).then((result, err) => {

		if (err){
			console.log(err)
			return false
		} else {
			return result
		}
	})
}

module.exports.createTask = (requestBody) => {


	//Creates a task object based on the Mongoos model "task"
	let newTask = new Task({
		//sets the "name" property with the value received from the client 
		name: requestBody.name


	});

	//the first paramater will store the result return by the Mongoose save method
	//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {

		if(error){
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if (err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}


module.exports.updateTask = (taskId, reqBody) => {

	return Task.findByIdAndUpdate(taskId).then((result, err) =>{
		if(err) {
			console.log(err)
			return false
		} 

		result.status = reqBody.status;

		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){
				console.log(saveErr)
			}else {
				return updatedTask
			}
		})
	})
}